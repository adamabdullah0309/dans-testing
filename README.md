CARA PENGGUNAAN API BACKEND
1. Jalankan query untuk mendapatkan struktur db pada folder src/main/java/resources/db/20220726181423.sql
2. Setelah menjalankan query diharapkan untuk signup terlebih dahulu menggunakan contoh json yang telah disediakan pada folder src/main/java/resources/json/signup.json
3. Setelah mendaftarkan user, maka bisa dilanjutkan dengan menggunakan swagger dengan url http://localhost:8080/swagger-ui/index.html/
4. Setelah masuk pada menu swagger maka isi kolom pencarian dengan /v3/api-docs, lalu klik explore
5. Setelah muncul list api pertama - tama lakukan login pada pai signin dengan username "mod2" dan password "12345678" 
6. Kemudian copy isi dari accessToken, lalu pilih tombol authorize kemudian muncul modal dan isi value dengan accessToken yang telah dicopy
7. Setelah itu bisa menggunakan semua api secara bebas
8. Untuk mengakses API positions maka pilih jobs controller