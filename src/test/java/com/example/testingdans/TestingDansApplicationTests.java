package com.example.testingdans;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import net.sf.json.JSONSerializer;

@SpringBootTest
class TestingDansApplicationTests {

	@Test
	void contextLoads() {
		String uri = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
		RestTemplate rest = new RestTemplate();
		String result = rest.getForObject(uri, String.class);

		ResponseEntity<String> result2 = rest.exchange(uri, HttpMethod.GET, null, String.class);
		JSONObject hasilVerivy = (JSONObject) JSONSerializer.toJSON(result2.getBody());
		System.out.print(hasilVerivy.toString());
		System.out.print("halo");
	}

}
