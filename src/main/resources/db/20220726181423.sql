/*
PostgreSQL Backup
Database: dans-testing/public
Backup Time: 2022-07-26 18:14:24
*/

DROP SEQUENCE IF EXISTS "public"."roles_id_seq";
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
DROP TABLE IF EXISTS "public"."roles";
DROP TABLE IF EXISTS "public"."user_roles";
DROP TABLE IF EXISTS "public"."users";
CREATE SEQUENCE "roles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
CREATE TABLE "roles" (
  "id" int4 NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
  "name" varchar(20) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "roles" OWNER TO "postgres";
CREATE TABLE "user_roles" (
  "user_id" int8 NOT NULL,
  "role_id" int4 NOT NULL
)
;
ALTER TABLE "user_roles" OWNER TO "postgres";
CREATE TABLE "users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "password" varchar(120) COLLATE "pg_catalog"."default",
  "username" varchar(20) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "users" OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."roles" IN SHARE MODE;
DELETE FROM "public"."roles";
INSERT INTO "public"."roles" ("id","name") VALUES (1, 'ROLE_USER'),(2, 'ROLE_MODERATOR'),(3, 'ROLE_ADMIN');
COMMIT;
BEGIN;
LOCK TABLE "public"."user_roles" IN SHARE MODE;
DELETE FROM "public"."user_roles";
COMMIT;
BEGIN;
LOCK TABLE "public"."users" IN SHARE MODE;
DELETE FROM "public"."users";
COMMIT;
ALTER TABLE "roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("id");
ALTER TABLE "user_roles" ADD CONSTRAINT "user_roles_pkey" PRIMARY KEY ("user_id", "role_id");
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
ALTER TABLE "user_roles" ADD CONSTRAINT "fkh8ciramu9cc9q3qcqiv4ue8a6" FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "user_roles" ADD CONSTRAINT "fkhfh9dx7w3ubf1co1vdev94g3f" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "users" ADD CONSTRAINT "ukr43af9ap4edm43mmtq01oddj6" UNIQUE ("username");
ALTER TABLE "users" ADD CONSTRAINT "uk6dotkott2kjsp8vw4d0m25fb7" UNIQUE ("email");
ALTER SEQUENCE "roles_id_seq"
OWNED BY "roles"."id";
SELECT setval('"roles_id_seq"', 2, false);
ALTER SEQUENCE "roles_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "users_id_seq"
OWNED BY "users"."id";
SELECT setval('"users_id_seq"', 2, true);
ALTER SEQUENCE "users_id_seq" OWNER TO "postgres";
