package com.example;

import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.parameters.Parameter;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TestingDansApplication.class);
	}

	@Bean
	public OperationCustomizer customize() {
		return (operation, handlerMethod) -> operation.addParametersItem(
				new Parameter()
						.in("header")
						.required(true)
						.description("myCustomHeader")
						.name("myCustomHeader"));
	}

}
